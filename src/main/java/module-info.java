module com.bekwam.javafxhyperlinkdialog {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.bekwam.javafxhyperlinkdialog to javafx.fxml;
    exports com.bekwam.javafxhyperlinkdialog;
}