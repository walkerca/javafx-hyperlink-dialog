package com.bekwam.javafxhyperlinkdialog;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Controller for hyperlinks-view.fxml
 *
 * Gathers validated input from two text fields: Text and URL.  These are the
 * components of a Hyperlink.
 *
 * @author carl
 */
public class HyperlinkDialogController {

    private final static int MAX_TEXT_LENGTH = 255;

    @FXML
    private TextField tfText;

    @FXML
    private TextField tfURL;

    @FXML
    private Label lblTextErr;

    @FXML
    private Label lblURLErr;

    /**
     * tfTextTouched and tfURLTouched are properties that delay required error
     * messages until the user interacts with the dialog
     */

    private BooleanProperty tfTextTouched = new SimpleBooleanProperty();

    private BooleanProperty tfURLTouched = new SimpleBooleanProperty();

    /**
     * A property indicating the form values are valid
     */
    private BooleanProperty valid = new SimpleBooleanProperty();

    public void initialize() {

        /**
         * Validation bindings for text
         */
        tfText.textProperty().addListener((obs,ov,nv) -> tfTextTouched.set(true));

        var lblTextErrBinding = Bindings.createStringBinding(() -> validateText(tfText.getText()), tfText.textProperty());

        lblTextErr.textProperty().bind(lblTextErrBinding);
        lblTextErr.visibleProperty().bind(lblTextErrBinding.isNotEmpty().and(tfTextTouched));

        /**
         * Validation bindings for url
         */
        tfURL.textProperty().addListener((obs,ov,nv) -> tfURLTouched.set(true));

        var lblURLErrBinding = Bindings.createStringBinding(() -> validateURL(tfURL.getText()), tfURL.textProperty());

        lblURLErr.textProperty().bind(lblURLErrBinding);
        lblURLErr.visibleProperty().bind(lblURLErrBinding.isNotEmpty().and(tfURLTouched));

        valid.bind(lblTextErrBinding.isEmpty().and(lblURLErrBinding.isEmpty()));
    }

    public void setInitialValue(Hyperlink initialValue) {

        /**
         * Set initial values (if needed)
         */
        if( initialValue != null ) {
            tfText.setText(initialValue.text());
            tfURL.setText(initialValue.url());
        }
    }

    public Hyperlink getHyperlink() {
        return new Hyperlink(
                tfText.getText(),
                tfURL.getText()
        );
    }

    protected String validateText(String text) {
        if( text == null || text.isEmpty() ) {
            return "Text is required";
        } else if (text.length() > MAX_TEXT_LENGTH ) {
            return "Text must be " + MAX_TEXT_LENGTH + " or fewer";
        }
        return "";
    }

    protected String validateURL(String url) {
        if( url == null || url.isEmpty() ) {
            return "URL is required";
        }
        try {
            new URL(url);
        } catch (MalformedURLException exc) {
            return "Not a valid URL";
        }
        return "";
    }

    /**
     * Bind to this property to determine if the form is valid
     *
     * Intended for use with external dialog button bar's save button
     *
     * @return
     */
    public BooleanProperty validProperty() { return valid; }
}
