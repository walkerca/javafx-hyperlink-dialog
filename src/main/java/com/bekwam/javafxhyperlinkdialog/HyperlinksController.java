package com.bekwam.javafxhyperlinkdialog;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;

/**
 * TableView-based screen demonstrating interaction with custom Dialog
 * HyperlinkDialog
 *
 * Uses showAndWait() without an initialValue when adding
 *
 * Uses showAndWait() with an initialValue (a row in the TableView) when
 * editing
 *
 * This controller also features a standard dialog "TextInputDialog" used to
 * change a Label at the top of this screen
 *
 * @author carl
 */
public class HyperlinksController {

    private final static String DEFAULT_TITLE = "My Hyperlinks";

    @FXML
    private TableView<Hyperlink> tvHyperlinks;

    @FXML
    private TableColumn<Hyperlink, String> tcText;

    @FXML
    private TableColumn<Hyperlink, String> tcURL;

    @FXML
    private Label lblTitle;

    private final StringProperty title = new SimpleStringProperty(DEFAULT_TITLE);

    public void initialize() {
        tcText.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().text()));
        tcURL.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().url()));

        tvHyperlinks.setRowFactory( tv -> {
            TableRow<Hyperlink> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Hyperlink rowData = row.getItem();
                    try {
                        new HyperlinkDialog(rowData).showAndWait().ifPresent(response -> {
                            var selectedIndex = tvHyperlinks.getSelectionModel().getSelectedIndex();
                            tvHyperlinks.getItems().set(selectedIndex, response);
                        });
                    } catch(IOException exc) {
                        exc.printStackTrace();
                    }
                }
            });
            return row ;
        });

        lblTitle.textProperty().bind(title);
    }

    @FXML
    protected void onNewHyperlink() {
        try {
            new HyperlinkDialog().showAndWait().ifPresent(response -> tvHyperlinks.getItems().add(response));
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    @FXML
    protected void onChangeTitle() {
        var dialog = new TextInputDialog(title.get());
        dialog.setTitle("Change Title");
        dialog.setHeaderText("Enter a new title");
        dialog.showAndWait().ifPresent(title::set);
    }
}
