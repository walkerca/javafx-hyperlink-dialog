package com.bekwam.javafxhyperlinkdialog;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Demonstrates the usage of a custom Dialog to gather several pieces of
 * information from the user and return them to the caller as a singular
 * result
 *
 * @author carl
 */
public class HyperlinkDialogApplication extends Application {

    private final static int WINDOW_HEIGHT = 568;
    private final static int WINDOW_WIDTH = 320;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HyperlinkDialogApplication.class.getResource("hyperlinks-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), WINDOW_HEIGHT, WINDOW_WIDTH);
        stage.setTitle("Hyperlink Dialog Demo");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
