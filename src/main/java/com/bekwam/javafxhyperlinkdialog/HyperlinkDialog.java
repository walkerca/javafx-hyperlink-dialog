package com.bekwam.javafxhyperlinkdialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;

/**
 * Custom dialog to return a validated pair of values -- Text and URL --
 * from TextField controls
 *
 * @author carl
 */
public class HyperlinkDialog extends Dialog<Hyperlink>  {

    public HyperlinkDialog() throws IOException {
        this(null);
    }

    /**
     * HyperlinkDialog can be pre-filled with values from initialValue or
     * left blank when initialValue is null
     *
     * @param initialValue allows null
     * @throws IOException
     */
    public HyperlinkDialog(Hyperlink initialValue) throws IOException {
        super();

        FXMLLoader fxmlLoader = new FXMLLoader(HyperlinksController.class.getResource("hyperlink-dialog.fxml"));

        ButtonType saveButtonType = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        this.setTitle("Hyperlink");
        this.setHeaderText("Enter hyperlink values");
        this.getDialogPane().setContent(fxmlLoader.load());

        HyperlinkDialogController c = fxmlLoader.getController();

        c.setInitialValue(initialValue); // null safe

        this.setResultConverter(p -> {
            if( p == saveButtonType ) {
                return c.getHyperlink();
            } else {
                return null;
            }
        });

        this
                .getDialogPane()
                .getButtonTypes()
                .addAll(saveButtonType, cancelButtonType);

        this.getDialogPane().lookupButton(saveButtonType).disableProperty().bind(c.validProperty().not());
    }
}
