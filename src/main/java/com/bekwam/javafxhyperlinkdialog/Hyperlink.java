package com.bekwam.javafxhyperlinkdialog;

/**
 * A Hyperlink domain object
 *
 * @param text
 * @param url
 */
public record Hyperlink(String text, String url) {}

